import 'package:app_api_test1/model/hospital_list_item.dart';
import 'package:flutter/material.dart';

class ComponentListItem extends StatelessWidget {
  const ComponentListItem({
    super.key,
    required this.item,
    required this.callback
  });

  final HospitalListItem item;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        padding: const EdgeInsets.all(10),
        margin:  const EdgeInsets.only(left: 10, right: 10 ,bottom: 10),
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(color: const Color.fromRGBO(100, 100, 100, 100)),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Row(
              children: [
                const Icon(Icons.add, size: 15,),
                Text(
                  '${item.orgnm == null ? '병원 이름이 없습니다.' : item.orgnm}',
                  style: const TextStyle(
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ],
            ),
            Text(
              '${item.slrYmd == null ? '기준일이 없습니다.' : '-${item.slrYmd}'}',
            ),
            Text(
              '${item.dywk == null ? '기준일 날짜가 없습니다.' : '-${item.dywk}'}',
            ),
          ],
        ),
      ),
    );
  }
}
