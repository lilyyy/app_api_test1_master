import 'package:app_api_test1/config/config_api.dart';
import 'package:app_api_test1/model/hospital_list.dart';
import 'package:dio/dio.dart';

class RepoHospitalList {
  final String _baseUrl = 'https://api.odcloud.kr/api/apnmOrg/v1/list?page={page}&perPage={perPage}&serviceKey=dEwAqrrgiUcCdt7d5yrhzBdqh%2FzoM7iN45D55Bm5z2XFaFDNyW06c%2FiMnKoJLCnlefaHJJf54nxeMKI3Wp0H3A%3D%3D';

  Future<HospitalList> getList({int page = 1, int perPage = 10, String searchArea = '전체'}) async {
    String _resultUrl = _baseUrl.replaceAll('{page}', page.toString());
    _resultUrl = _baseUrl.replaceAll('{perPage}', perPage.toString());

    if (searchArea != '전체') {
      _resultUrl = _resultUrl + '&cond%5BorgZipaddr%3A%3ALIKE%5D=${Uri.encodeFull(searchArea)}';
    }

    Dio dio = Dio();

    final response = await dio.get(
      _resultUrl,
      options: Options(
        followRedirects: false,
        validateStatus: (status) {
          if (status == 200) {
            return true;
          } else {
            return false;
          }
        }
      ),
    );

    return HospitalList.fromJson(response.data);
  }
}